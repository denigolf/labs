﻿using System;
using System.Text;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Default;
            Menu();
        }
        
        // Допоміжні методи
        public static void Menu()
        {
            Console.WriteLine("*** Лабораторна работа №3 ***");
            Console.WriteLine("Виберіть пункт меню:");
            Console.WriteLine("Заповнити область масиву:");
            Console.WriteLine("1. псевдовипадковими числами.");
            Console.WriteLine("2. парними/непарними числами в діапазоні від n до m.");
            Console.WriteLine("3. випадковими символами.");
            Console.WriteLine("4. випадковими літерами англійського алфавіту.");
            Console.WriteLine("----------------------------");
            Console.WriteLine("Пошук в області масиву:");
            Console.WriteLine("5. max/min.");
            Console.WriteLine("6. Заданого символа.");
            Console.WriteLine("----------------------------");
            Console.WriteLine("7. Транспонувати масив.");
            Console.WriteLine("8. Заповнити масив псевдовипадковими числами. (Область 1)");
            Console.WriteLine("<Enter> - Вихід.");

            string option = Console.ReadLine();

            if (option == "")
            {
                return;
            }

            switch (option)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("Заповнити область масиву:");
                        Console.WriteLine("1. псевдовипадковими числами.");
                        
                        PrintAskArraySize();
                        int[,] array1 = new int[GetInt(),GetInt()];
                        FillArrayWithRandomNumbers(array1);
                        PrintArray(array1);
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("Заповнити область масиву:");
                        Console.WriteLine("2. парними/непарними числами в діапазоні від n до m.");
                        
                        Console.WriteLine("Введіть n: ");
                        int n = GetInt();
                        
                        Console.WriteLine("Введіть m: ");
                        int m = GetInt();
                        
                        Console.WriteLine("Введіть режим (1 - заповнити парними, 2 - заповнити непарними): ");
                        int mode = GetInt();

                        if (mode == 1)
                        {
                            PrintAskArraySize();
                            int[,] array2 = new int[GetInt(),GetInt()];
                            FillArrayWithEvenOrOddNumbers(true, n, m, array2);
                            PrintArray(array2);
                        }
                        else if (mode == 2)
                        {
                            PrintAskArraySize();
                            int[,] array2 = new int[GetInt(),GetInt()];
                            FillArrayWithEvenOrOddNumbers(false, n, m, array2);
                            PrintArray(array2);
                        }
                        else
                        {
                            Console.WriteLine("Такого режиму не існує.");
                        }

                        PressAnyKey();
                        Menu();
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("Заповнити область масиву:");
                        Console.WriteLine("3. випадковими символами.");
                        
                        PrintAskArraySize();
                        char[,] array3 = new char[GetInt(),GetInt()];
                        FillArrayWithRandomSymbols(array3);
                        PrintCharArray(array3);

                        PressAnyKey();
                        Menu();
                        break;
                    case "4":
                        Console.Clear();
                        Console.WriteLine("Заповнити область масиву:");
                        Console.WriteLine("4. випадковими літерами англійського алфавіту.");
                        
                        PrintAskArraySize();
                        char[,] array4 = new char[GetInt(),GetInt()];
                        FillArrayWithRandomEnglishLetters(array4);
                        PrintCharArray(array4);
                  
                        PressAnyKey();
                        Menu();
                        break;
                    case "5":
                        Console.Clear();
                        Console.WriteLine("Пошук в області масиву:");
                        Console.WriteLine("5. max/min.");
                        
                        PrintAskArraySize();
                        int[,] array5 = new int[GetInt(),GetInt()];
                        FillArrayWithRandomNumbers(array5);
                        Console.WriteLine("Отриманий масив:");
                        PrintArray(array5);
                        Console.WriteLine($"Максимальне значення: {FindMaxOrMinInArray(true, array5)}");
                        Console.WriteLine($"Мінімальне значення: {FindMaxOrMinInArray(false, array5)}");
                  
                        PressAnyKey();
                        Menu();
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine("Пошук в області масиву:");
                        Console.WriteLine("6. Заданого символа.");
                        
                        PrintAskArraySize();
                        char[] array6 = new char[GetInt()];
                        FillArrayWithRandomEnglishLetters(array6);
                        PrintCharArray(array6);
                        PrintAskChar();
                        Console.WriteLine($"\nСимвол знайдено на позиції: {FindCharInArray(GetChar(), array6)}");

                        PressAnyKey();
                        Menu();
                        break;
                    case "7":
                        Console.Clear();
                        Console.WriteLine("7. Транспонувати масив.");
                        PrintAskArraySize();
                        int[,] array7 = new int[GetInt(),GetInt()];
                        FillArrayWithRandomNumbers(array7);
                        Console.WriteLine("Отриманий масив:");
                        PrintArray(array7);
                        int[,] transposeArray = GetTransposeArray(array7);
                        Console.WriteLine("Транспонований масив:");
                        PrintArray(transposeArray);
                  
                        PressAnyKey();
                        Menu();
                        break;
                    case "8":
                        Console.Clear();
                        Console.WriteLine("8. Заповнити масив псевдовипадковими числами. (Область 1)");
                        
                        PrintAskArraySize();
                        int[,] array8 = new int[GetInt(),GetInt()];
                        FillArrayWithRandomNumbersInArea1(array8);
                        
                        PressAnyKey();
                        Menu();
                        break;
                    default:
                        Console.WriteLine("Такого пункту меню не існує!");
                        
                        PressAnyKey();
                        Menu();
                        break;
                }
        }

        // Допоміжні методи
        public static void PrintAskArraySize()
        {
            Console.WriteLine("Введіть розмір масиву: ");
        }
        
        public static void PrintAskChar()
        {
            Console.WriteLine("Введіть символ: ");
        }
        
        public static void PrintArray(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                { 
                    Console.Write(arr[i, j] + "  ");
                    
                }
                Console.WriteLine();
            }
        }
        
        public static void PrintArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + "\t");
            }
        }

        public static void PrintCharArray(char[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                { 
                    Console.Write(arr[i, j] + "  ");
                    
                }
                Console.WriteLine();
            }
        }
        
        public static void PrintCharArray(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + "\t");
            }
            Console.WriteLine();
        }
        
        public static int GetInt()
        {
            string value = Console.ReadLine();
            int num = Convert.ToInt32(value);
            return num;
        }

        public static char GetChar()
        {
            char value = Console.ReadKey().KeyChar;
            return value;
        }
        
        public static void PressAnyKey()
        {
            Console.WriteLine("\nНатисніть будь-яку клавішу, щоб повернутись назад...");
            Console.ReadLine();
            Console.Clear();
          
        }
        
        // Заполнение области массива:
        //1. Псевдослучайными числами.
        public static void FillArrayWithRandomNumbers(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Random random = new Random();
                    int randomNumber = random.Next(1, 100);

                    arr[i, j] = randomNumber;
                }
               
            }
        }
        
        // Псевдослучайными числами. (Область 1)
        public static void FillArrayWithRandomNumbersInArea1(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < i+1; j++)
                { 
                    Random rnd = new Random();
                int value = rnd.Next(1, 9);

                arr[i,j] = value;
                }
            }
            
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                { 
                    Console.Write(arr[i,j] + "  ");
                    
                }
                Console.WriteLine();
            }
           
           
        }

        //2. Четными/нечетными числами в диапазоне от n до m.
        public static void FillArrayWithEvenOrOddNumbers(bool isEven, int n, int m, int[,] arr)
        {
            if (isEven)
            {
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    for (int j = 0; j < arr.GetLength(1); j++)
                    {
                        Random random = new Random();
                        int randomNumber = random.Next(n, m);
                        if (randomNumber % 2 == 0)
                        {
                            arr[i, j] = randomNumber;
                        }
                        else
                        {
                            arr[i, j] = randomNumber + 1;
                        }
                    }
                }
            } else
            {
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    for (int j = 0; j < arr.GetLength(1); j++)
                    {
                        Random rnd = new Random();
                        int value = rnd.Next(n, m);
                        if (value % 2 != 0)
                        {
                            arr[i, j] = value;
                        }
                        else
                        {
                            arr[i, j] = value + 1;
                        }
                    }
                }
            }
        }

        //3. Любыми символами.
        public static void FillArrayWithRandomSymbols(char[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Random random = new Random();
                    char randomSymbol = (char) random.Next(1, 500);

                    arr[i, j] = randomSymbol;
                }
            }
        }

        //4. Любыми буквами англ.алфавита.
        public static void FillArrayWithRandomEnglishLetters(char[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Random random = new Random();
                    Random log = new Random();
                    
                    int randomLogNumber = log.Next();
                    if (randomLogNumber % 2 == 0)
                    {
                        char randomLetter = (char) random.Next(65, 90);
                        arr[i, j] = randomLetter;
                    }
                    else
                    {
                        char randomLetter = (char) random.Next(97, 122);
                        arr[i, j] = randomLetter;
                    }
                }
            }
        }

        // Для одновимірного масиву
        public static void FillArrayWithRandomEnglishLetters(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Random random = new Random();
                Random logRandom = new Random();
                int randomLogNumber = logRandom.Next();
                if (randomLogNumber % 2 == 0)
                {
                    char randomLetter = (char) random.Next(65, 90);
                    arr[i] = randomLetter;
                }
                else
                {
                    char randomLetter = (char) random.Next(97, 122);
                    arr[i] = randomLetter;
                }
            }
        }

        //Поиск в области массива:
        //5. max/min.
        // mode = true - максимальне
        // mode = false - мінімальне
        public static int FindMaxOrMinInArray(bool mode, int [,] arr)
        {
            if (mode)
            {
                int maxValue = -99999;
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    for (int j = 0; j < arr.GetLength(1); j++)
                    {
                        if (arr[i, j] > maxValue)
                        {
                            maxValue = arr[i, j];
                        }
                    }
                }
                return maxValue;
            } else
            {
                int minValue = 99999;
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    for (int j = 0; j < arr.GetLength(1); j++)
                    {
                        if (arr[i, j] < minValue)
                        {
                            minValue = arr[i, j];
                        }
                    }
                }
                return minValue;
            }
        }

        //6. Заданного символа.
        public static int FindCharInArray(char value, char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == value)
                {
                    return i;
                }
            } 
            
            return -1;
            

            /*int count = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == value)
                    {
                        return value;
                    }
                }
            }

            int[,] resArr = new int[count, 2];
            
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == value)
                    {
                        resArr[i, j] = i;
                        resArr[i, j+1] = j;
                    }
                }
            }
            
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.WriteLine($"resArr[{i}, {j}] = [{resArr[i,j]}]");
                }
            }

            return -1;
            */
        }

        //7. Транспонировать массив.
        public static int[,] GetTransposeArray(int[,] arr)
        {
            int[,] result = new int[arr.GetLength(1), arr.GetLength(0)];

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    result[j, i] = arr[i, j];
                }
            }
            return result;
        }
        
        //8. Переписать элементы из одной области в другую.
    }
}
