﻿using System;
using System.Numerics;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Default;
            Menu();
        }

        public static void Menu()
        {
            Console.WriteLine("*** Лабораторна работа №1 ***");
            Console.WriteLine("Виберіть пункт меню:");
            Console.WriteLine("1. Кількість цифр в числі N.");
            Console.WriteLine("2. Організувати масив, елементами котрого будуть цифри числа N.");
            Console.WriteLine("3. Середнє арифметичне цифр числа N.");
            Console.WriteLine("4. Середнє геометричне цифр числа N.");
            Console.WriteLine("5. Факторіал N!.");
            Console.WriteLine("6. Сума всіх парних чисел від 1 до N.");
            Console.WriteLine("7. Сума всіх непарных чисел від 1 до N.");
            Console.WriteLine("8. Сума всіх парных чисел від N1 до N2.");
            Console.WriteLine("9. Сума всіх непарных чисел від N1 до N2.");
            Console.WriteLine("<Enter> - Вихід.");
            
           
            
            string option = Console.ReadLine();

            if (option == "")
            {
                return;
            }

            switch (option)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("1. Кількість цифр в числі N.");
                        AskInt();
                        Console.WriteLine("Результат: " + GetCountNumberOfDigits(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("2. Організувати масив, елементами котрого будуть цифри числа N.");
                        AskInt();
                        int [] arr = CreateArrayWithElementsOfNumber(GetInt());
                        PrintArray(arr);
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("3. Середнє арифметичне цифр числа N.");
                        AskInt();
                        Console.WriteLine("Результат: " + GetArithmeticMeanOfDigits(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "4":
                        Console.Clear();
                        Console.WriteLine("4. Середнє геометричне цифр числа N.");
                        AskInt();
                        Console.WriteLine("Результат: " + GetGeometricMeanOfDigits(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "5":
                        Console.Clear();
                        Console.WriteLine("5. Факторіал N!.");
                        AskInt();
                        Console.WriteLine("Результат: " + GetFactorial(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine("6. Сума всіх парних чисел від 1 до N. (Реалізовано усіма конструкціями циклів)");
                        AskInt();
                        Console.WriteLine("Результат: " + GetSumOfEvenDigitsFromOneToNumberFor(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "7":
                        Console.Clear();
                        Console.WriteLine("7. Сума всіх непарних чисел від 1 до N.");
                        AskInt();
                        Console.WriteLine("Результат: " + GetSumOfOddDigitsFromOneToNumber(GetInt()));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "8":
                        Console.Clear();
                        Console.WriteLine("8. Сума всіх парних чисел від N1 до N2.");
                        Console.WriteLine("Введіть перше ціле число N1: ");
                        int num1 = GetInt();
                        Console.WriteLine("Введіть друге ціле число N2: ");
                        int num2 = GetInt();
                        Console.WriteLine("Результат: " + GetSumOfEvenDigitsFromOneToNumber(num1, num2));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "9":
                        Console.Clear();
                        Console.WriteLine("9. Сума всіх непарних чисел від N1 до N2.");
                        Console.WriteLine("Введіть перше ціле число N1: ");
                        int num3 = GetInt();
                        Console.WriteLine("Введіть друге ціле число N2: ");
                        int num4 = GetInt();
                        Console.WriteLine("Результат: " + GetSumOfOddDigitsFromOneToNumber(num3, num4));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    default:
                        Console.WriteLine("Такого пункту меню не існує!");
                        
                        PressAnyKey();
                        Menu();
                        break;
                }
        }
        
        // Допоміжні методи
        public static void PressAnyKey()
        {
            Console.WriteLine("\nНатисніть будь-яку клавішу, щоб повернутись назад...");
            Console.ReadLine();
            Console.Clear();
          
        }
        
        public static int GetInt()
        {
            string value = Console.ReadLine();
            int num = Convert.ToInt32(value);
            return num;
        }
        public static void AskInt()
        {
            Console.WriteLine("Введіть ціле число N: ");
        }

        public static void PrintArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"arr[{i}]: {arr[i]}");
            }
        }
        
        // 1. Количество цифр в числе N
        public static int GetCountNumberOfDigits(int num)
        {
            int count = 0;
            while (num > 0)
            {
                num /= (int)10;
                count++;
            }
            
            return count;
        }
        
        // 2. Организовать массив, элементами которого будут цифры числа N
        public static int[] CreateArrayWithElementsOfNumber(int num)
        {
            string str = num.ToString();
            int[] arr = new int[str.Length];

            for (int i = 0; i < str.Length; i++)
            {
                arr[i] = Convert.ToInt32(str[i].ToString());
            }

            return arr;
        }

        // 3. Среднее арифметическое цифр числа N.
        public static double GetArithmeticMeanOfDigits(int num)
        {
            int[] arr = CreateArrayWithElementsOfNumber(num);
            int numLength = GetCountNumberOfDigits(num);
            long sum = 0;
            double result = 0;
            
            for (int i = 0; i < numLength; i++)
            {
                sum += arr[i];
            }
            
            result = sum / numLength;
            
            return result;
        }
        
        // 4. Среднее геометрическое цифр числа N.
        public static double GetGeometricMeanOfDigits(int num)
        {
            int[] arr = CreateArrayWithElementsOfNumber(num);
            int numLength = GetCountNumberOfDigits(num);
            long multiplication = 1;
            double result = 0;
            
            for (int i = 0; i < numLength; i++)
            {
                multiplication *= arr[i];
            }
          
            result = Math.Round(Math.Pow(multiplication, 1.0 / numLength), 6);
            return result;
        }
        
        // 5. Факториал N!.
        public static BigInteger GetFactorial(int num)
        {
            BigInteger result;
            if (num == 1)
            {
                return num;
            }

            result = num * GetFactorial(num - 1);

            return result;
        }
        
        // 6. Суму всех парных чисел от 1 до N.
        // Реалізація через For
        public static long GetSumOfEvenDigitsFromOneToNumberFor(int num)
        {
            long result = 0;
            for (int i = 1; i <= num; i++)
            {
                if (i % 2 == 0)
                {
                    result += i;
                }
            }
            
            return result;
        }
        
        // Реалізація через While
        public static long GetSumOfEvenDigitsFromOneToNumberWhile(int num)
        {
            long result = 0;
            int i = 0;
            
            while (i <= num)
            {
                if (i % 2 == 0)
                {
                    result += i;
                }

                i++;
            }
            
            return result;
        }
        
        // Реалізація через do while
        public static long GetSumOfEvenDigitsFromOneToNumberDoWhile(int num)
        {
            long result = 0;
            int i = 0;
            do
            {
                if (i % 2 == 0)
                {
                    result += i;
                }

                i++;
            } while (i <= num);
           
            return result;
        }
        
        // 7. Суму всех парных чисел от 1 до N. (Перегрузка)
        public static long GetSumOfEvenDigitsFromOneToNumber(int num1, int num2)
        {
            long result = 0;
            while (num1 <= num2)
            {
                if (num1 % 2 == 0)
                {
                    result += num1;
                } 
                num1++;
            }
            
            return result;
        }
        
        // 8. Суму всех непарных чисел от N1 до N2.
        public static long GetSumOfOddDigitsFromOneToNumber(int num)
        {
            long result = 0;
            for (int i = 1; i <= num; i++)
            {
                if (i % 2 != 0)
                {
                    result += i;
                }
            }
            
            return result;
        }
        
        // 9. Суму всех непарных чисел от N1 до N2. (Перегрузка)
        public static long GetSumOfOddDigitsFromOneToNumber(int num1, int num2)
        {
            long result = 0;
            while (num1 <= num2)
            {
                if (num1 % 2 != 0)
                {
                    result += num1;
                } 
                
                num1++;
            }
            return result;
        }

    }
}