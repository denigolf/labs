﻿using System;
using System.Text;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Default;
            Menu();
        }
        
         public static void Menu()
        {
            Console.WriteLine("*** Лабораторна работа №2 ***");
            Console.WriteLine("Виберіть пункт меню:");
            Console.WriteLine("1. Чи містить Str1 всі символи Str2.");
            Console.WriteLine("2. Замінити всі символи в Str1 рівні символам з Str2 на символ #.");
            Console.WriteLine("3. Скільки разів Str2 зустрічається в Str1.");
            Console.WriteLine("<Enter> - Вихід.");

            string option = Console.ReadLine();

            if (option == "")
            {
                return;
            }

            switch (option)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("1. Чи містить Str1 всі символи Str2.");
                        PrintAskStr(1);
                        string str1ForIsContains = GetStr();
                        PrintAskStr(2);
                        string str2ForIsContains = GetStr();
                        Console.WriteLine("Результат: " + IsStr1ContainsAllCharsOfStr2(str1ForIsContains, str2ForIsContains));
                        
                        PressAnyKey();
                        Menu();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("2. Замінити всі символи в Str1 рівні символам з Str2 на символ #.");
                        PrintAskStr(1);
                        StringBuilder str1ForReplace = new StringBuilder(GetStr());
                        PrintAskStr(2);
                        StringBuilder str2ForReplace = new StringBuilder(GetStr());
                        ReplaceAllCharsInStr1EqualToCharsFromStr2WithHash(str1ForReplace, str2ForReplace);
                        Console.WriteLine("Результат: " + str1ForReplace);

                        PressAnyKey();
                        Menu();
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("3. Скільки разів Str2 зустрічається в Str1.");
                        PrintAskStr(1);
                        string str1ForGetCount = GetStr();
                        PrintAskStr(2);
                        string str2ForGetCount = GetStr();
                        Console.WriteLine("Результат: " + GetCountOfStr2InStr1(str1ForGetCount, str2ForGetCount));

                        PressAnyKey();
                        Menu();
                        break;
                    default:
                        Console.WriteLine("Такого пункту меню не існує!");
                        
                        PressAnyKey();
                        Menu();
                        break;
                }
        }
         
         // Допоміжні методи
         public static void PressAnyKey()
         {
             Console.WriteLine("\nНатисніть будь-яку клавішу, щоб повернутись назад...");
             Console.ReadLine();
             Console.Clear();
          
         }

         public static void PrintAskStr(int num)
         {
             Console.WriteLine($"Введіть Str{num}: ");
         }

         public static string GetStr()
         {
             string str = Console.ReadLine();
             return str;
         }

        //Содержит ли Str1 все символы Str2;
        public static bool IsStr1ContainsAllCharsOfStr2(string str1, string str2)
        {
            bool isContainsChar = false;

            for (int i = 0; i < str2.Length; i++)
            {
                isContainsChar = false;
                
                for (int j = 0; j < str1.Length; j++)
                {
                    if (str2[i] == str1[j])
                    {
                        isContainsChar = true;
                        break;
                    }
                }
                
                if(!isContainsChar)
                {
                    break;
                }
            }

            return isContainsChar;
        }
        
        //Заменить все символы в Str1 равные символам из Str2 на символ #;
        public static void ReplaceAllCharsInStr1EqualToCharsFromStr2WithHash(StringBuilder str1, StringBuilder str2)
        {
            for (int i = 0; i < str1.Length; i++)
            {
                for (int j = 0; j < str2.Length; j++)
                {
                    if (str1[i] == str2[j])
                    {
                        str1[i] = '#';
                    }
                }
            }
        }
        
        // Определить сколько раз Str2 встречается в Str1.
        public static int GetCountOfStr2InStr1(string str1, string str2)
        {
            int count = 0;
            for (int i = 0; i < str1.Length; i++)
            {
                if (i + str2.Length > str1.Length)
                {
                    break;
                }
                  
                bool isFound = true;

                for (int j = 0; j < str2.Length; j++)
                {
                    if (str2[j] != str1[i + j])
                    {
                        isFound = false;
                    }
                }

                if (isFound)
                {
                    count++;
                    i = i + str2.Length;
                }
            }
            
            return count;
        }
    }
}


